#!/bin/bash

# Auto configure script for Kali Linux
# Author: skycracker
# Version: 2.0

### Script configuration

# Color settings 
RED="\033[01;31m"      # Issues/Errors
GREEN="\033[01;32m"    # Success
YELLOW="\033[01;33m"   # Warnings/Information
BLUE="\033[01;34m"     # Heading
BOLD="\033[01;01m"     # Highlight
RESET="\033[00m"       # Normal

VERSION=2
LOGFILE=/tmp/cfgkali/log.txt
TOR=true

### Root checking -- run this script as root
if [[ $EUID -ne 0 ]]; then
	echo -e $RED'[!] This script must be run as root. Quitting...'$RESET
	exit 1
fi

### Print heading
clear
echo -e $BLUE""
echo -e "Kali Confiuration Script"
echo -e "------------------------"
echo -e "by "$RED"skycracker"$BLUE", Version "$VERSION
echo -e ""$RESET


### Creating Log File
mkdir /tmp/cfgkali
echo "" > $LOGFILE

### ---- CONFIGURE AND UPDATE KALI ---- ###
echo -e $BLUE"[i] Beginning wih kali configuration ..."$RESET


### Shut down services
echo -e $BLUE"[i] Shutting down services."$RESET
service metasploit stop
service postgresql stop


### Updateing operating system
echo -e $BLUE"[i] Updating operating system. This may take some time."$RESET

apt-get -q update
apt-get -y -q install kali-archive-keyring

for FILE in clean autoremove;
    do apt-get -y -q "$FILE"; # Clean up      clean remove autoremove autoclean
done

echo -e $GREEN"[+] Update started ..."$RESET

export DEBIAN_FRONTEND=noninteractive
apt-get -q update && apt-get -y -q dist-upgrade --fix-missing
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] Operating system updated."$RESET
else
    echo -e $RED"[-] OS Update failed."$RESET
    echo "[-] OS Update failed." >> $LOGFILE
fi


### Installing kernel headers
echo -e $BLUE"[i] Installing kernel headers"$RESET
apt-get -y -q install gcc make "linux-headers-$(uname -r)"
if [[ $? -ne 0 ]]; then
    echo -e $RED"[-] There was an issue installing kernel headers."$RESET
    echo -e $YELLOW"[!] Are you using the latest kernel?"$RESET
    echo "[-] There was an issue installing kernel headers. Are you using the latest kernel?" >> $LOGFILE
fi
apt-get -y -q update


### Install all KALI tools
echo -e $BLUE"[i] Installing all default tools."$RESET
apt-get -y -q install kali-linux-full
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] Default tools installed."$RESET
else
    echo -e $RED"[-] Default kalo tool installation failed."$RESET
    echo "[-] Default kalo tool installation failed." >> $LOGFILE
fi


### Enable auto-login
echo -e $GREEN"[+] Auto-login enabled"$RESET
sed -i 's/#  AutomaticLoginEnable/   AutomaticLoginEnable/' /etc/gdm3/daemon.conf
sed -i 's/#  AutomaticLogin/   AutomaticLogin/' /etc/gdm3/daemon.conf


### Updating Expoit-DB
#cd /usr/share/exploitdb
#rm -rf archive.tar.bz2
#wget http://www.exploit-db.com/archive.tar.bz2
#tar xvfj archive.tar.bz2
#rm -rf archive.tar.bz2
#echo -e $GREEN"[+] Local exploit database updated"$RESET


### ---- INSTALL ADDITIONAL SOFTWARE ---- ###
echo -e $BLUE"[i] Installing additional softeware ..."$RESET

### Install File Roller (Archive Manager)
apt-get -y -q install unrar unace rar unrar p7zip zip unzip p7zip-full p7zip-rar file-roller 
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] File Roller installed."$RESET
else
	echo -e $RED"[-] File Roller installation failed."$RESET
	echo "[-] File Roller installation failed." >> $LOGFILE
fi

### Install curl
apt-get -y -q install curl
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] curl installed."$RESET
else
	echo -e $RED"[-] curl installation failed."$RESET
	echo "[-] curl installation failed." >> $LOGFILE
fi

### Install bridge-utils - bridge network interfaces
apt-get -y -q install bridge-utils
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] bridge-utils installed."$RESET
else
	echo -e $RED"[-] bridge-utils installation failed."$RESET
	echo "[-] bridge-utils installation failed." >> $LOGFILE
fi

### Install GDebi
apt-get -y -q install gdebi
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] GDebi installed."$RESET
else
	echo -e $RED"[-] GDebi installation failed."$RESET
	echo "[-] GDebi installation failed." >> $LOGFILE
fi

### Install vfeed - database
apt-get -y -q install vfeed
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] vfeed installed."$RESET
else
	echo -e $RED"[-] vfeed installation failed."$RESET
	echo "[-] vfeed installation failed." >> $LOGFILE
fi

### Install pwgen - password generator
apt-get install -y -q pwgen
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] pwgen installed."$RESET
else
	echo -e $RED"[-] pwgen installation failed."$RESET
	echo "[-] pwgen installation failed." >> $LOGFILE
fi

### Installing httptunnel - tunnels data streams in HTTP requests
apt-get -y -q install http-tunnel
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] httptunnel installed."$RESET
else
	echo -e $RED"[-] httptunnel installation failed."$RESET
	echo "[-] httptunnel installation failed." >> $LOGFILE
fi


### Installing sshuttle - VPN over SSH
apt-get -y -q install sshuttle
#--- Example
#sshuttle --dns --remote root@123.9.9.9 0/0 -vv
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] sshuttle installed."$RESET
else
	echo -e $RED"[-] sshuttle installation failed."$RESET
	echo "[-] sshuttle installation failed." >> $LOGFILE
fi


### Installing iodine - DNS tunneling (IP over DNS)
apt-get -y -q install iodine
#--- Example
#iodined -f -P password1 10.0.0.1 dns.mydomain.com
#iodine -f -P password1 123.9.9.9 dns.mydomain.com; ssh -C -D 8081 root@10.0.0.1
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] iodine installed."$RESET
else
	echo -e $RED"[-] iodine installation failed."$RESET
	echo "[-] iodine installation failed." >> $LOGFILE
fi


### Installing dns2tcp - DNS tunneling (TCP over DNS)
apt-get -y -q install dns2tcp
#file=/etc/dns2tcpd.conf; [ -e "$file" ] && cp -n $file{,.bkup}; echo -e "listen = 0.0.0.0\nport = 53\nuser = nobody\nchroot = /tmp\ndomain = dnstunnel.mydomain.com\nkey = password1\nressources = ssh:127.0.0.1:22" > "$file"; dns2tcpd -F -d 1 -f /etc/dns2tcpd.conf
#file=/etc/dns2tcpc.conf; [ -e "$file" ] && cp -n $file{,.bkup}; echo -e "domain = dnstunnel.mydomain.com\nkey = password1\nresources = ssh\nlocal_port = 8000\ndebug_level=1" > "$file"; dns2tcpc -f /etc/dns2tcpc.conf 178.62.206.227; ssh -C -D 8081 -p 8000 root@127.0.0.1
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] dns2tcp installed."$RESET
else
	echo -e $RED"[-] dns2tcp installation failed."$RESET
	echo "[-] dns2tcp installation failed." >> $LOGFILE
fi

### Installing ptunnel - IMCP tunneling
apt-get -y -q install ptunnel
#--- Example
#ptunnel -x password1
#ptunnel -x password1 -p 123.9.9.9 -lp 8000 -da 127.0.0.1 -dp 22; ssh -C -D 8081 -p 8000 root@127.0.0.1
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] ptunnel installed."$RESET
else
	echo -e $RED"[-] ptunnel installation failed."$RESET
	echo "[-] ptunnel installation failed." >> $LOGFILE
fi


### Installing stunnel - SSL wrapper
apt-get -y -q install stunnel
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] stunnel installed."$RESET
else
	echo -e $RED"[-] stunnel installation failed."$RESET
	echo "[-] stunnel installation failed." >> $LOGFILE
fi
# Remove from start up
update-rc.d -f stunnel4 remove

### Install gcc & multilib
echo -e $GREEN"[+] Installing gcc & multilibc - compiling libraries."$RESET
for FILE in cc gcc g++ gcc-multilib make automake libc6 libc6-dev libc6-amd64 libc6-dev-amd64 libc6-i386 libc6-dev-i386 libc6-i686 libc6-dev-i686 build-essential dpkg-dev; do
  apt-get -y -q install "$FILE" 2>/dev/null
done


### Installing backdoor factory
#apt-get -y -qq install backdoor-factory
#if [[ $? -ne 0 ]]; then
#    echo -e $GREEN"[+] Backdoor Factory installed."$RESET
#else
#	echo -e $RED"[-] Backdoor Factory installation failed."$RESET
#	echo "[-] Backdoor Factory installation failed." >> $LOGFILE
#fi

### Install shellter - dynamic shellcode injector
apt-get -y -q install shellter
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] shellter installed."$RESET
else
	echo -e $RED"[-] shellter installation failed."$RESET
	echo "[-] shellter installation failed." >> $LOGFILE
fi

### Install firmware-mod-kit
apt-get -y -q install firmware-mod-kit
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] firmware-mod-kit installed."$RESET
else
	echo -e $RED"[-] firmware-mod-kit installation failed."$RESET
	echo "[-] firmware-mod-kit installation failed." >> $LOGFILE
fi

### Install shellnoob - shellcode writing toolkit
apt-get -y -q install shellnoob
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] shellnoob installed."$RESET
else
	echo -e $RED"[-] shellnoob installation failed."$RESET
	echo "[-] shellnoob installation failed." >> $LOGFILE
fi

### Install gitg - GUI git client
apt-get -y -q install gitg
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] GUI git client installed."$RESET
else
	echo -e $RED"[-] GUI git client installation failed."$RESET
	echo "[-] GUI git client installation failed." >> $LOGFILE
fi

### Install hashid
apt-get -y -q install hashid
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] hashid installed."$RESET
else
	echo -e $RED"[-] hashid installation failed."$RESET
	echo "[-] hashid installation failed." >> $LOGFILE
fi


### Install Flash
apt-get -y -qq install flashplugin-nonfree
update-flashplugin-nonfree --install
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] Flash installed."$RESET
else
	echo -e $RED"[-] Flash installation failed."$RESET
	echo "[-] Flash installation failed." >> $LOGFILE
fi

### Install aircrack-ng -  Wi-Fi cracking suite
apt-get -y -q install aircrack-ng
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] aircrack-ng installed."$RESET
else
	echo -e $RED"[-] aircrack-ng installation failed."$RESET
	echo "[-] aircrack-ng installation failed." >> $LOGFILE
fi
# Setup hardware database
mkdir -p /etc/aircrack-ng/
airodump-ng-oui-update 2>/dev/null || curl --progress -k -L "http://standards.ieee.org/develop/regauth/oui/oui.txt" > /etc/aircrack-ng/oui.txt          #***!!! hardcoded path!
[ -e /etc/aircrack-ng/oui.txt ] && (\grep "(hex)" /etc/aircrack-ng/oui.txt | sed 's/^[ \t]*//g;s/[ \t]*$//g' > /etc/aircrack-ng/airodump-ng-oui.txt)
# Setup alias
file=/root/.bash_aliases; [ -e "$file" ] && cp -n $file{,.bkup}   #/etc/bash.bash_aliases
([[ -e "$file" && "$(tail -c 1 $file)" != "" ]]) && echo >> "$file"
grep -q '^## aircrack-ng' "$file" 2>/dev/null || echo -e '## aircrack-ng\nalias aircrack-ng="aircrack-ng -z"\n' >> "$file"
grep -q '^## airodump-ng' "$file" 2>/dev/null || echo -e '## airodump-ng \nalias airodump-ng="airodump-ng --manufacturer --wps --uptime"\n' >> "$file"    # aircrack-ng 1.2 rc2



### BeEF - cross-site scripting framework
#apt-get -y -qq install beef-xss
#if [[ $? -ne 0 ]]; then
#    echo -e $GREEN"[+] BeEF installed."$RESET#
#else
#	echo -e $RED"[-] BeEF installation failed."$RESET
#	echo "[-] BeEF installation failed." >> $LOGFILE
#fi

#Install Tor
echo -e $GREEN"[+] Installing Tor and setting up anonymus proxy."$RESET
apt-get -y -q install tor 
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] Tor installed."$RESET
else
	$TOR=false
	echo -e $RED"[-] Tor installation failed."$RESET
	echo "[-] Tor installation failed." >> $LOGFILE
fi

apt-get -y -q install privoxy
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] Privoxy installed."$RESET
else
	$TOR=false
	echo -e $RED"[-] Privoxy installation failed."$RESET
	echo "[-] Privoxy installation failed." >> $LOGFILE
fi

apt-get -y -q install vidalia
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] Vidalia installed."$RESET
else
	$TOR=false
	echo -e $RED"[-] Vidalia installation failed."$RESET
	echo "[-] Vidalia installation failed." >> $LOGFILE
fi 

echo "listen-address   127.0.0.1:8118" >> /etc/privoxy/config
echo "forward-socks4	/	127.0.0.1:9050 ." >> /etc/privoxy/config
echo "forward-socks4a   /	127.0.0.1:9050 ." >> /etc/privoxy/config
echo "forward-socks5	/	127.0.0.1:9050 ." >> /etc/privoxy/config

service tor restart 
service privoxy restart 
update-rc.d tor enable
update-rc.d privoxy enable

if [ $TOR ]; then 
	echo -e $GREEN"[+] Proxy service installed and ready on port 8118 (for http) and 9050 (for socket)"$RESET
else
	echo -e $RED"[!] Could not install proxy service on port 8118 (for http) and 9050 (for socket)"$RESET
fi

### ---- DOWNLOAD GIT & SCRIPT SOFTWARE ---- ###
echo -e $BLUE"[i] Downloading non-repro software from GIT and other sources ..."$RESET

mkdir /git

### Backdoor-Factory-Proxy
apt-get -y -q install python-capstone
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] python-capstone installed."$RESET
else
	echo -e $RED"[-] python-capstone installation failed."$RESET
	echo "[-] python-capstone installation failed." >> $LOGFILE
fi
echo -e "\e[01;32m[+] Downloading Backdoor-Factory-Proxy ...\e[00m"
mkdir /git/bdfproxy
git clone https://github.com/secretsquirrel/BDFProxy /git/bdfproxy
cd /tmp/cfgkali
wget https://pefile.googlecode.com/files/pefile-1.2.10-139.tar.gz
tar zxvf pefile-1.2.10-139.tar.gz
chmod 777 /tmp/cfgkali/pefile-1.2.10-139/setup.py
cd /tmp/cfgkali/pefile-1.2.10-139/
./setup.py install || echo "[-] PeFile installation failed." >> $LOGFILE
cd /git/bdfproxy
chmod 777 install.sh
chmod 777 update.sh 
echo -e $GREEN"[+] Installing Backdoor-Factory-Proxy ..."$RESET
./install.sh || echo "[-] BDFProxy installation failed." >> $LOGFILE
echo -e $GREEN"[+] Backdoor-Factory-Proxy installed"$RESET

### HTTPScreenshot 
echo -e $GREEN"[+] Downloading HTTPScreenShot."$RESET
mkdir /git/httpscreenshot
pip install -q selenium
git clone https://github.com/breenmachine/httpscreenshot.git /git/httpscreenshot
cd /git/httpscreenshot
chmod +x install-dependencies.sh && ./install-dependencies.sh

### SMBExec
echo -e $GREEN"[+] Downloading SMBExec."$RESET
mkdir /git/smbexec
git clone https://github.com/brav0hax/smbexec /git/smbexec

### Masscan
echo -e $GREEN"[+] Downloading Masscan."$RESET
apt-get -y -q install git gcc make libpcap-dev
mkdir /git/masscan
git clone https://github.com/robertdavidgraham/masscan.git /git/masscan
cd /git/masscan
make
make install

### Eyewitness
echo -e $GREEN"[+] Downloading Eyewitness."$RESET
mkdir /git/eyewitness
git clone https://github.com/ChrisTruncer/EyeWitness.git /git/eyewitness

### Printer Exploits
echo -e $GREEN"[+] Downloading Printer Exploits."$RESET
mkdir /git/praedasploit
git clone https://github.com/MooseDojo/praedasploit /git/praedasploit

### Recon-ng
echo -e $GREEN"[+] Downloading Recon-ng."$RESET
mkdir /git/recon-ng
git clone https://bitbucket.org/LaNMaSteR53/recon-ng.git /git/recon-ng

### Responder
echo -e $GREEN"[+] Downloading Responder."$RESET
mkdir /git/responder
git clone https://github.com/SpiderLabs/Responder.git /git/responder

### shellcode_retriever
echo -e $GREEN"[+] Downloading Shellcode Retriever."$RESET
mkdir /git/shellcode_retriever
git clone https://github.com/secretsquirrel/shellcode_retriever /git/shellcode_retriever

### Discover Scripts - Passive reconnaissance
echo -e $GREEN"[+] Installing Discover Scripts."$RESET
mkdir /git/discover
git clone https://github.com/leebaird/discover.git /git/discover
cd /git/discover
./setup.sh || echo "[-] Discover Scripts installation failed." >> $LOGFILE

### Veil - Create Python based Meterpreter executable
#echo -e $GREEN"[+] Installing Veil Framework."$RESET
#mkdir /git/veil
#git clone https://github.com/Veil-Framework/Veil-Evasion.git /git/veil
#cd /git/veil/setup
#chmod 777 setup.sh
#./setup.sh || echo "[-] Veil installation failed." >> LOGFILE

### PowerSploit - Scripts for post exploitation
echo -e $GREEN"[+] Installing PowerSploit."$RESET
mkdir /git/PowerSploit
git clone https://github.com/mattifestation/PowerSploit.git /git/PowerSploit
cd /git/PowerSploit
wget https://raw.githubusercontent.com/obscuresec/random/master/StartListener.py
wget https://raw.githubusercontent.com/darkoperator/powershell_scripts/master/ps_encoder.py

### WCE (Windows Credential Editor) - Pulls passwords from memory
echo -e $GREEN"[+] Downloading and installing WCE (Windows Credential Editor)."$RESET
mkdir /git/wce
cd /tmp/
wget http://www.ampliasecurity.com/research/wce_v1_41beta_universal.zip
unzip -d /git/wce/ wce_v1_41beta_universal.zip
rm -f wce_v1_41beta_universal.zip

### Mimikatz - Pulls passwords from memory
echo -e $GREEN"[+] Installing Mimikatz."$RESET
mkdir /git/mimikatz
cd /tmp
wget https://github.com/gentilkiwi/mimikatz/releases/download/2.0.0-alpha-20150727/mimikatz_trunk.7z
7za x mimikatz_trunk.7z -o/git/mimikatz/
rm -f mimikatz_trunk.7z

### PeepingTom - Website snapshots
echo -e $GREEN"[+] Installing PeepingTom."$RESET
mkdir /git/peepingtom
git clone https://bitbucket.org/LaNMaSteR53/peepingtom.git /git/peepingtom
cd /git/peepingtom/
wget https://gist.githubusercontent.com/nopslider/5984316/raw/423b02c53d225fe8dfb4e2df9a20bc800cc78e2c/gnmap.pl

### Download appropriate PhantomJS package
echo -e $GREEN"[+] Installing PhantomJS package."$RESET
#if $(uname -m | grep '64'); then
	cd /tmp/cfgkali
    wget http://phantomjs.googlecode.com/files/phantomjs-1.9.2-linux-x86_64.tar.bz2
    tar xf phantomjs-1.9.2-linux-x86_64.tar.bz2
    cp /git/peepingtom/phantomjs-1.9.2-linux-x86_64/bin/phantomjs .
#else
#    wget http://phantomjs.googlecode.com/files/phantomjs-1.9.2-linux-i686.tar.bz2
#    tar xf phantomjs-1.9.2-linux-i686.tar.bz2
#    cp /git/peepingtom/phantomjs-1.9.2-linux-i686/bin/phantomjs
#fi

### Nmap script - Quicker scanning and smarter identification
echo -e $GREEN"[+] Installing nmap scripts."$RESET
cd /usr/share/nmap/scripts/
wget https://raw.githubusercontent.com/hdm/scan-tools/master/nse/banner-plus.nse


### Bypassuac - Used to bypass UAC in post exploitation
echo -e $GREEN"[+] Installing Bypass UAC."$RESET
cd /tmp/cfgkali
wget https://www.trustedsec.com/files/bypassuac.zip
unzip bypassuac.zip
cp bypassuac/bypassuac.rb /opt/metasploit/apps/pro/msf3/scripts/meterpreter/
mv bypassuac/uac/ /opt/metasploit/apps/pro/msf3/data/exploits/
rm -Rf bypassuac

### PEDA - Python Exploit Development Assistance for GDB
echo -e $GREEN"[+] Installing PEDA."$RESET
git clone https://github.com/longld/peda.git /opt/peda
echo "source /opt/peda/peda.py" >> ~/.gdbinit

### DSHashes
echo -e $GREEN"[+] Downloading DSHashes."$RESET
mkdir /opt/NTDSXtract
wget http://ptscripts.googlecode.com/svn/trunk/dshashes.py -O /opt/NTDSXtract/dshashes.py

### SPARTA
echo -e $GREEN"[+] Downloading SPARTA."$RESET
mkdir /git/sparta
git clone https://github.com/secforce/sparta.git /git/sparta
apt-get -y -q update
apt-get -y -q install python-elixir
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] python-elixir installed."$RESET
else
	echo -e $RED"[-] python-elixir installation failed."$RESET
	echo "[-] python-elixir installation failed." >> $LOGFILE
fi
apt-get -y -q update
apt-get -y -q install nmap hydra cutycapt ldap-utils rwho rsh-client x11-apps finger
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] SPARTA Dependencies installed."$RESET
else
	echo -e $RED"[-] SPARTA Dependencies installation failed."$RESET
	echo "[-] SPARTA Dependencies installation failed." >> $LOGFILE
fi

### NoSQLMap
echo -e $GREEN"[+] Downloading NoSQLMap."$RESET
mkdir /git/nosqlmap
git clone https://github.com/tcstool/NoSQLMap.git /git/nosqlmap

### Nishang
echo -e $GREEN"[+] Downloading Nishang."$RESET
mkdir /git/nishang
git clone https://github.com/samratashok/nishang /git/nishang

### Net-Creds Network Parsing
echo -e $GREEN"[+] Downloading Net-Creds Network Parsing."$RESET
mkdir /git/net-creds
git clone https://github.com/DanMcInerney/net-creds.git /git/net-creds

### Wifite
echo -e $GREEN"[+] Downloading Wifite."$RESET
mkdir /git/wifite
git clone https://github.com/derv82/wifite /git/wifite

### WIFIPhisher
echo -e $GREEN"[+] Downloading WIFIPhisher."$RESET
mkdir /git/wifiphisher
git clone https://github.com/sophron/wifiphisher.git /git/wifiphisher

### Spiderfoot
echo -e $GREEN"[+] Downloading Spiderfoot."$RESET
mkdir /opt/spiderfoot/ && cd /opt/spiderfoot
wget http://sourceforge.net/projects/spiderfoot/files/spiderfoot-2.3.0-src.tar.gz/download
tar xzvf download
pip install -q lxml
pip install -q netaddr
pip install -q M2Crypto
pip install -q cherrypy
pip install -q mako

### Installing wpscan (GIT)
#echo -e $GREEN"[+] Installing wpscan"
#mkdir /git/wpscan
#git clone git://github.com/wpscanteam/wpscan.git /git/wpscan
#pushd /git/wpscan >/dev/null
#git pull
#popd >/dev/null

### Installing sqlmap (GIT)
#echo -e $GREEN"[+] Installing sqlmap"$RESET
#mkdir /git/wpscan
#git clone git://github.com/sqlmapproject/sqlmap.git mkdir /git/wpscan
#pushd /git/wpscan >/dev/null
#git pull
#popd >/dev/null

### ---- DOWNLOADING ADDITIONAL SCRIPTS ---- ###
echo -e $BLUE"[i] Downloading additional scripts ... "$RESET

### The Hacker Playbook 2 - Custom Scripts
echo -e $GREEN"[+] Downloading THP2 - Custom Scripts."$RESET
mkdir /git/thp2
git clone https://github.com/cheetz/Easy-P.git /git/thp2/Easy-P
git clone https://github.com/cheetz/Password_Plus_One /git/thp2/Password_Plus_One
git clone https://github.com/cheetz/PowerShell_Popup /git/thp2/PowerShell_Popup
git clone https://github.com/cheetz/icmpshock /git/thp2/icmpshock
git clone https://github.com/cheetz/brutescrape /git/thp2/brutescrape
git clone https://www.github.com/cheetz/reddit_xss /git/thp2/reddit_xss

### The Hacker Playbook 2 - Forked Versions
echo -e $GREEN"[+] Downloading THP2 - Forked Versions."$RESET
git clone https://github.com/cheetz/PowerSploit /git/thp2/HP_PowerSploit
git clone https://github.com/cheetz/PowerTools /git/thp2/HP_PowerTools
git clone https://github.com/cheetz/nishang /git/thp2/nishang

### ---- APP CONFIG ---- ###
echo -e $BLUE"[i] Configure services ... "$RESET

#autostart metasploit & postgreslq
echo -e $GREEN"[+] Auto-start for metasploit activated"$RESET
service postgresql start
service metasploit start
update-rc.d postgresql enable
update-rc.d metasploit enable

#clean apt-get
echo -e $GREEN"[+] Removing all unessesary packages."$RESET
apt-get update && apt-get upgrade -y -q && apt-get dist-upgrade -y -q || echo "[-] 2nd Operating System Update failed." >> $LOGFILE
apt-get autoremove -y -q && apt-get -y -q autoclean || echo "[-] Package cleaning failed." >> $LOGFILE

updatedb

#End message
echo -e $BLUE"[i] Kali has been configurated."$RESET
echo -e $YELLOW"[!] Don't forget to change the proxy settings to 127.0.0.1:8118"$RESET

echo -e $RED
cat $LOGFILE
echo -e $RESET
