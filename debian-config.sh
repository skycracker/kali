#!/bin/bash

# Auto configure script for Debian Linux
# Author: skycracker
# Version: 2.0

### Script configuration

# Color settings
RED="\033[01;31m"      # Issues/Errors
GREEN="\033[01;32m"    # Success
YELLOW="\033[01;33m"   # Warnings/Information
BLUE="\033[01;34m"     # Heading
BOLD="\033[01;01m"     # Highlight
RESET="\033[00m"       # Normal

VERSION=3
LOGFILE=/tmp/cfgdebian/log.txt
BASEDIR=$(dirname $0)

### Root checking -- run this script as root
if [[ $EUID -ne 0 ]]; then
	echo -e $RED'[!] This script must be run as root. Quitting...'$RESET
	exit 1
fi

### Print heading
clear
echo -e $BLUE""
echo -e "Debian Confiuration Script"
echo -e "--------------------------"
echo -e "by "$RED"skycracker"$BLUE", Version "$VERSION
echo -e ""$RESET


### Creating Log File
mkdir /tmp/cfgdebian
echo "" > $LOGFILE

### ---- CONFIGURE AND UPDATE KALI ---- ###
echo -e $BLUE"[i] Beginning wih Debian configuration ..."$RESET


# Copy config files
echo -e $BLUE"[i] Copy config files"$RESET
cp .conkyrc /root/

### Updateing operating system
echo -e $BLUE"[i] Updating operating system. This may take some time."$RESET

apt-get -q update

for FILE in clean autoremove;
    do apt-get -y -q "$FILE"; # Clean up      clean remove autoremove autoclean
done

echo -e $GREEN"[+] Update started ..."$RESET

export DEBIAN_FRONTEND=noninteractive
apt-get -q update && apt-get -y -q dist-upgrade --fix-missing
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] Operating system updated."$RESET
else
    echo -e $RED"[-] OS Update failed."$RESET
    echo "[-] OS Update failed." >> $LOGFILE
fi


### Installing kernel headers
echo -e $BLUE"[i] Installing kernel headers"$RESET
apt-get -y -q install gcc make "linux-headers-$(uname -r)"
if [[ $? -ne 0 ]]; then
    echo -e $RED"[-] There was an issue installing kernel headers."$RESET
    echo -e $YELLOW"[!] Are you using the latest kernel?"$RESET
    echo "[-] There was an issue installing kernel headers. Are you using the latest kernel?" >> $LOGFILE
fi
apt-get -y -q update


### ---- INSTALL ADDITIONAL SOFTWARE ---- ###
echo -e $BLUE"[i] Installing additional softeware ..."$RESET

### Install Archive Managers
apt-get -y -q install unrar unace rar unrar p7zip zip unzip p7zip-full p7zip-rar
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] Archive Managers installed."$RESET
else
	echo -e $RED"[-] Archive Manager installation failed."$RESET
	echo "[-] Archive Manager installation failed." >> $LOGFILE
fi

### Install curl
apt-get -y -q install curl
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] curl installed."$RESET
else
	echo -e $RED"[-] curl installation failed."$RESET
	echo "[-] curl installation failed." >> $LOGFILE
fi

### Install bridge-utils - bridge network interfaces
apt-get -y -q install bridge-utils
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] bridge-utils installed."$RESET
else
	echo -e $RED"[-] bridge-utils installation failed."$RESET
	echo "[-] bridge-utils installation failed." >> $LOGFILE
fi

### Install GDebi
apt-get -y -q install gdebi
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] GDebi installed."$RESET
else
	echo -e $RED"[-] GDebi installation failed."$RESET
	echo "[-] GDebi installation failed." >> $LOGFILE
fi

### Install gcc & multilib
echo -e $GREEN"[+] Installing gcc & multilibc - compiling libraries."$RESET
for FILE in cc gcc g++ gcc-multilib make automake libc6 libc6-dev libc6-amd64 libc6-dev-amd64 libc6-i386 libc6-dev-i386 libc6-i686 libc6-dev-i686 build-essential dpkg-dev; do
  apt-get -y -q install "$FILE" 2>/dev/null
done

### Install gitg - GUI git client
apt-get -y -q install gitg
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] GUI git client installed."$RESET
else
	echo -e $RED"[-] GUI git client installation failed."$RESET
	echo "[-] GUI git client installation failed." >> $LOGFILE
fi

### Install conky
apt-get -y -q install conky
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] Conky installed."$RESET
else
	echo -e $RED"[-] Conky installation failed."$RESET
	echo "[-] Conky installation failed." >> $LOGFILE
fi

### Install ddd - Data display debugger
apt-get -y -q install ddd
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] ddd installed."$RESET
else
	echo -e $RED"[-] ddd installation failed."$RESET
	echo "[-] ddd installation failed." >> $LOGFILE
fi

### Install valgrind - dynmaic analysis tools framework
apt-get -y -q install valgrind
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] valgrind installed."$RESET
else
	echo -e $RED"[-] valgrind installation failed."$RESET
	echo "[-] valgrind installation failed." >> $LOGFILE
fi

### Terminator (Terminal multiplexer) ###
echo -e $GREEN"[+] Installing Terminator."$RESET
apt-get -y -q install terminator
if [[ $? -eq 0 ]]; then
    echo -e $GREEN"[+] Terminator installed."$RESET
else
	echo -e $RED"[-] Terminator installation failed."$RESET
	echo "[-] Terminator installation failed." >> $LOGFILE
fi
cp $BASEDIR/.terminator  /root/.config/terminator/
mv /root/.config/terminator/.terminator /root/.config/terminator/config
cp $BASEDIR/.terminator  /home/patrick/.config/terminator/
mv /home/patrick/.config/terminator/.terminator /home/patrick/.config/terminator/config

### ---- INSTALLING DESIGN ---- #
echo -e $BLUE"[i] Installing visual designs ... "$RESET
cd /tmp/cfgkali/
wget http://skycracker.bplaced.net/src/kali2-style.zip
unzip kali2-style.zip
mv /tmp/cfgkali/kali2-style/Numix /usr/share/themes/Numix
mv /tmp/cfgkali/kali2-style/Vibrancy-Kali-Orange /usr/share/icons/Vibrancy-Kali-Orange
rm -Rf /tmp/cfgkali/kali2-style

echo -e $GREEN"[+] Activating themes for Gnome desktop."$RESET
gsettings set org.gnome.desktop.interface gtk-theme "Numix"
gsettings set org.gnome.desktop.wm.preferences theme "Numix"
gsettings set org.gnome.desktop.interface icon-theme "Vibrancy-Kali-Orange"


echo -e $GREEN"[+] Activating extensions for Gnome desktop."$RESET
gnome-shell-extension-tool -e apps-menu@gnome-shell-extensions.gcampax.github.com
gnome-shell-extension-tool -e user-theme@gnome-shell-extensions.gcampax.github.com
gnome-shell-extension-tool -e places-menu@gnome-shell-extensions.gcampax.github.com

#clean apt-get
echo -e $GREEN"[+] Removing all unessesary packages."$RESET
apt-get update && apt-get upgrade -y -q && apt-get dist-upgrade -y -q || echo "[-] 2nd Operating System Update failed." >> $LOGFILE
apt-get autoremove -y -q && apt-get -y -q autoclean || echo "[-] Package cleaning failed." >> $LOGFILE

updatedb

#End message
echo -e $BLUE"[i] Debain has been configurated."$RESET

echo -e $RED
cat $LOGFILE
echo -e $RESET
